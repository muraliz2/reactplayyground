import React, { Component } from 'react';
import { View, TextInput, Button, StyleSheet } from 'react-native';
import DefaultInput from '../UI/DefaultTextInput';

const placeInput = props => {
  return (
    <DefaultInput
      placeholder="place Name"
      value={props.placeName}
      onChangeText={props.onChangeText}
    />
  );
};

export default placeInput;
