import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

import DefaultButton from '../UI/DefaultButton';
import ImagePlaceHolder from '../../assets/beautiful-place.jpg';

class PickLocation extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.placeholder}>
          <Text>Map</Text>
        </View>
        <DefaultButton>locateme</DefaultButton>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center'
  },
  placeholder: {
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor: '#eee',
    width: '80%',
    height: 150
  },
  ImagePlaceHolder: {
    width: '100%',
    height: '100%'
  }
});

export default PickLocation;
