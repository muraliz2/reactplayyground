import React, { Component } from 'react';
import {
  View,
  Text,
  Button,
  TextInput,
  StyleSheet,
  ImageBackground
} from 'react-native';
import DefaultInput from '../../components/UI/DefaultTextInput';
import HeadingText from '../../components/UI/HeadingText';
import MainText from '../../components/UI/MainText/MainText';
import imageBackground from '../../assets/background.jpg';

import startMainTabs from '../MainTabs/startMainTabs';
import DefaultButton from '../../components/UI/DefaultButton';

class AuthScreen extends Component {
  loginHandler = () => {
    startMainTabs();
  };

  render() {
    return (
      <ImageBackground style={styles.imageBackground} source={imageBackground}>
        <View style={styles.container}>
          <MainText>
            <HeadingText>Please Register</HeadingText>
          </MainText>
          <DefaultButton color="#29aaf4" onPress={() => console.log('hi')}>
            Switch to login
          </DefaultButton>
          <View style={styles.inputContainer}>
            <DefaultInput
              placeholder="your email address"
              style={styles.input}
            />
            <DefaultInput placeholder="password" style={styles.input} />
            <DefaultInput placeholder="confirm password" style={styles.input} />
          </View>
          <DefaultButton color="#29aaf4" onPress={this.loginHandler}>
            Submit
          </DefaultButton>
        </View>
      </ImageBackground>
    );
  }
}

export default AuthScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
    // borderColor: 'red',
    // borderWidth: 2
  },
  inputContainer: {
    width: '80%'
  },
  imageBackground: {
    flex: 1,
    width: '100%'
  },
  input: {
    backgroundColor: '#eee',
    borderColor: '#bbb'
  }
});
